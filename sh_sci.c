/*
 *  TOPPERS/JSP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Just Standard Profile Kernel
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2001-2003 by Industrial Technology Institute,
 *                              Miyagi Prefectural Government, JAPAN
 *  Copyright (C) 2002-2004 by Hokkaido Industrial Research Institute, JAPAN
 *  Copyright (C) 2012- by Monami-ya LLC, Japan
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 *  @(#) $Id: sh7145sci.c,v 1.3 2005/07/06 00:45:07 honda Exp $
 */

/*
 *   SH SCI用 簡易SIOドライバ 
 *   　
 *   　アーキテクチャ依存部に置かずに、pdic/shディレクトリに置いている
 *   　のは、sh1/2aとsh3/4aで共有するため
 */

#include <sil.h>
#include "target_syssvc.h"
#include "sh_sci.h"

#if defined(TNUM_SIOP_SH_SCI)
#undef TNUM_SIOP
#define TNUM_SIOP TNUM_SIOP_SH_SCI
#endif /* TNUM_SIOP_SH_SCI */

/*
 * SH SCIのレジスタ設定
 */

/*
 *  コントロールレジスタのアドレスのオフセット 
 */
#define SCI_SCR	0x2U
#define SCI_SSR	0x4U
#define SCI_BRR	0x1U
#define SCI_SMR	0x0U
#define SCI_TDR	0x3U
#define SCI_RDR	0x5U
#define SCI_SDCR 0x6U


/*
 *  コントロールレジスタの設定値
 */
#define SCI_TIE		0x80U		/* トランスミットインタラプトイネーブル */
#define SCI_RIE		0x40U		/* レシーブインタラプトイネーブル   */
#define SCI_TE		0x20U		/* トランスミットイネーブル         */
#define SCI_RE		0x10U		/* レシーブイネーブル           */
#define SSR_ORER	0x20U		/* オーバーランエラー           */
#define SSR_FER		0x10U		/* フレーミングエラー           */
#define SSR_PER		0x08U		/* パリティエラー           */
#define SSR_TDRE	0x80U		/* トランスミットデータレジスタエンプティ */
#define SSR_RDRF	0x40U		/* レシーブデータレジスタフル       */

#define sh_sci_DELAY 	105000U

#define PFC_TXD0	0x0004U
#define PFC_RXD0	0x0001U
#define PFC_TXD1	0x0100U
#define PFC_RXD1	0x0040U
#define PFC_TXD2	0x0020U
#define PFC_RXD2	0x8000U

/*
 *  SCIの設定
 */
#define SMR_CKS 	0x0U			/*  分周比          */
#define BRR9600 	79U			/*  9600 ビットレート    */
#define BRR19200	39U			/*  19200 ビットレート    */
#define BRR38400	19U			/*  38400 ビットレート    */

/*
 *  シリアルI/Oポート初期化ブロック
 */
typedef struct sio_port_initialization_block
{
	uint32_t reg_base;				/* レジスタのベースアドレス */
	uint8_t brr;						/* ボーレートの設定値   */
	uint8_t smr;						/* モードレジスタの設定値   */
	uint8_t int_level;				/* 割り込みレベルの設定値   */
} SIOPINIB;

/*
 *  シリアルI/Oポート管理ブロック
 */
struct sio_port_control_block
{
	const SIOPINIB *siopinib;	/* シリアルI/Oポート初期化ブロック */
	intptr_t exinf;				/* 拡張情報 */
	bool_t openflag;				/* オープン済みフラグ */
	bool_t sendflag;				/* 送信割込みイネーブルフラグ */
	bool_t getready;				/* 文字を受信した状態 */
	bool_t putready;				/* 文字を送信できる状態 */
};

/*
 * シリアルI/Oポート初期化ブロック
 */
const SIOPINIB siopinib_table[TNUM_PORT] = {
	{0xffff81b0, BRR9600, 0x0, 6}, /* SCI1 */
#if TNUM_PORT >= 2
	{0xffff81a0, BRR9600, 0x0, 6}, /* SCI0 */
#endif /* TNUM_PORT >= 2 */
};


/*
 *  シリアルI/Oポート管理ブロックのエリア
 */
static SIOPCB siopcb_table[TNUM_PORT];

/*
 *  シリアルI/OポートIDから管理ブロックを取り出すためのマクロ
 */
				/*  ポートIDからデバイス番号を求めるマクロ  */
#define INDEX_SIOP(siopid)	((uint_t)((siopid) - 1))
#define get_siopcb(siopid)	(&(siopcb_table[INDEX_SIOP(siopid)]))

/*
 *  文字を受信できるか？
 */
Inline bool_t
sh_sci_getready (SIOPCB * siopcb)
{
	/*  レシーブデータレジスタフル・フラグのチェック  */
	return (sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR)) &
			SSR_RDRF);
}

/*
 *  文字を送信できるか？
 */
Inline bool_t
sh_sci_putready (SIOPCB * siopcb)
{
	/*  トランスミットデータレジスタエンプティ・フラグのチェック */
	return (sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR)) &
			SSR_TDRE);
}

/*
 *  受信した文字の取出し
 */
Inline char_t
sh_sci_getchar (SIOPCB * siopcb)
{
	char_t data;

	data = sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_RDR));
	/*  レシーブデータレジスタフル・フラグのクリア  */
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR),
				 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
									  SCI_SSR)) & ~SSR_RDRF);
	return data;
}

/*
 *  送信する文字の書込み
 */
Inline void
sh_sci_putchar (SIOPCB * siopcb, char_t c)
{
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_TDR), c);
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR),
				 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
									  SCI_SSR)) & ~SSR_TDRE);
}

/*
 *  SIOドライバの初期化ルーチン
 */
void
sh_sci_initialize ()
{
	SIOPCB *siopcb;
	uint_t i;

	/*
	 *  シリアルI/Oポート管理ブロックの初期化
	 */
	for (siopcb = siopcb_table, i = 0; i < TNUM_PORT; siopcb++, i++) {
		siopcb->openflag = false;
		siopcb->siopinib = (&siopinib_table[i]);
	}
}

/*
 *  オープンしているポートがあるか？
 */
bool_t
sh_sci_openflag (ID siopid)
{
	return (siopcb_table[siopid - 1].openflag);
}

/*
 *  シリアルI/Oポートのオープン
 */
SIOPCB *
sh_sci_opn_por (ID siopid, intptr_t exinf)
{
	SIOPCB *siopcb = get_siopcb (siopid);

	/*  送受信停止  */
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR), 0x00);

	/*  SCIデータ入出力ポートの設定  */
	/*  ピンアサイン */
	/* sys_initializeで設定 */

	/*  送受信フォーマット  */
	/*  調歩同期式  */
	/*  8ビット、パリティなし  */
	/*  ストップビットレングス：1   */
	/*  クロックセレクト */

	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SMR),
				 siopcb->siopinib->smr);
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_BRR),
				 (uint8_t) siopcb->siopinib->brr);

	/*
	 * ボーレートの設定後、1カウント分待たなければならない。
	 */
	sil_dly_nse (sh_sci_DELAY);	/* 値はｓｈ１と同じ */

	/* エラーフラグをクリア */
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR),
				 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
									  SCI_SSR)) & ~(SSR_ORER | SSR_FER |
													SSR_PER));
	/*送受信許可、受信割り込み許可 */
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
				 (uint8_t) (SCI_RIE | SCI_TE | SCI_RE));

	siopcb->exinf = exinf;
	siopcb->openflag = true;
	return (siopcb);
}

/*
 *  シリアルI/Oポートのクローズ
 */
void
sh_sci_cls_por (SIOPCB * siopcb)
{
	/*  送受信停止、割込み禁止  */
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
				 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
									  SCI_SCR)) & (uint8_t) ~ (SCI_TIE | SCI_RIE |
														  SCI_TE | SCI_RE));
	siopcb->openflag = false;
}

/*
 *  シリアルI/Oポートへの文字送信
 */
bool_t
sh_sci_snd_chr (SIOPCB * siopcb, char_t c)
{
	if (sh_sci_putready (siopcb)) {
		sh_sci_putchar (siopcb, c);
		return (true);
	}
	return (false);
}

/*
 *  シリアルI/Oポートからの文字受信
 */
int_t
sh_sci_rcv_chr (SIOPCB * siopcb)
{
	if (sh_sci_getready (siopcb)) {
		return ((int_t) (uint8_t) sh_sci_getchar (siopcb));
		/*  (uint8_t)でキャストするのはゼロ拡張にするため  */
	}
	return (-1);
}

/*
 *  シリアルI/Oポートからのコールバックの許可
 */
void
sh_sci_ena_cbr (SIOPCB * siopcb, uint_t cbrtn)
{
	switch (cbrtn) {
	case SIO_ERDY_SND:			/* 送信割り込み要求を許可 */
		sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
					 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
										  SCI_SCR)) | SCI_TIE);
		break;
	case SIO_ERDY_RCV:			/* 受信割り込み要求を許可 */
		sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
					 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
										  SCI_SCR)) | SCI_RIE);
		break;
	}
}

/*
 *  シリアルI/Oポートからのコールバックの禁止
 */
void
sh_sci_dis_cbr (SIOPCB * siopcb, uint_t cbrtn)
{
	switch (cbrtn) {
	case SIO_ERDY_SND:			/* 送信割り込み要求を禁止 */
		sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
					 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
										  SCI_SCR)) & ~SCI_TIE);
		break;
	case SIO_ERDY_RCV:			/* 受信割り込み要求を禁止 */
		sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR),
					 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
										  SCI_SCR)) & ~SCI_RIE);
		break;
	}
}

/*
 *  シリアルI/Oポートに対する送信割込み処理
 */
Inline void
sh_sci_isr_siop_out (SIOPCB * siopcb)
{
	uint8_t scr0 = sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR));

	if ((scr0 & SCI_TIE) != 0 && sh_sci_putready (siopcb)) {
		/*
		 *  送信通知コールバックルーチンを呼び出す．
		 */
		sh_sci_ierdy_snd (siopcb->exinf);
	}
}

/*
 *  シリアルI/Oポートに対する受信割込み処理
 */
Inline void
sh_sci_isr_siop_in (SIOPCB * siopcb)
{
	uint8_t scr0 = sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SCR));

	if ((scr0 & SCI_RIE) != 0 && sh_sci_getready (siopcb)) {
		/*
		 *  受信通知コールバックルーチンを呼び出す．
		 */
		sh_sci_ierdy_rcv (siopcb->exinf);
	}
}

/*
 *  シリアルI/Oポートに対する受信エラー割込み処理
 */
Inline void
sh_sci_isr_siop_err (SIOPCB * siopcb)
{
	sil_wrb_mem ((uint8_t *) (siopcb->siopinib->reg_base + SCI_SSR),
				 sil_reb_mem ((uint8_t *) (siopcb->siopinib->reg_base +
									  SCI_SSR)) & ~(SSR_ORER | SSR_FER |
													SSR_PER));
}

/*
 *  SCI送信割込みサービスルーチン
 *  
 */
void
sh_sci_isr_out (ID siopid)
{
    SIOPCB	*p_siopcb = get_siopcb(siopid);
    sh_sci_isr_siop_out (p_siopcb);
}

/*
 *  SIO受信割込みサービスルーチン
 *  
 */
void
sh_sci_isr_in (ID siopid)
{
    SIOPCB	*p_siopcb = get_siopcb(siopid);
    sh_sci_isr_siop_in (p_siopcb);
}

/*
 *  SIO受信エラー割込みサービスルーチン
 */
void
sh_sci_isr_error (ID siopid)
{
    SIOPCB	*p_siopcb = get_siopcb(siopid);
    sh_sci_isr_siop_err (get_siopcb (1));
}

/*
 * ポーリングによる文字の送信
 */
void
sh_sci_putc_pol (ID portid, char_t c)
{
	while (!sh_sci_putready (&siopcb_table[portid - 1]));
	sh_sci_putchar (&siopcb_table[portid - 1], c);
}
